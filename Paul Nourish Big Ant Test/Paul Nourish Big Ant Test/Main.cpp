#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>

#include "Knight.h"
#include "CombatManager.h"
#include "Equipment.h"
#include "OppositionKnight.h"



bool runProgram;

std::string playerName;

std::vector<OppositionKnight> allOppositionKnights;
std::vector<Equipment> allEquipment;

Knight playerCharacter;

CombatManager combatManager;


//basic Menu System with various options
void ReadInputFile(std::string fileToReadName)
{
	std::string line;
	std::ifstream myfile(fileToReadName);
	if (myfile.is_open())
	{
		std::cout << std::endl << std::endl;

		while (std::getline(myfile, line))
		{
			std::cout << line << '\n';
		}
		std::cout << std::endl << std::endl;
		myfile.close();
	}

	else std::cout << "Unable to open file";
}


//create and write to a file with a specified name and message.
void WriteFile(std::string fileToWrite, std::string messageToWrite)
{
	std::ofstream myfile;
	myfile.open(fileToWrite);
	myfile << messageToWrite;
	myfile.close();
}
//prints the player values from a specified line to screen.
void readInSpecificLine(std::string fileToReadName, int lineToRead)
{
	std::string line;
	std::ifstream myfile(fileToReadName);
	if (myfile.is_open())
	{
		for (int i = 0; i < lineToRead; i++)
		{
			std::getline(myfile, line);
			if(i == (lineToRead - 1))
			{
				std::cout << line;
			}
		}
		myfile.close();
	}

	else std::cout << "Unable to open file";

}
//creates a string containing one line from a csv file.
std::string createStringWithSingleLine(std::string fileToReadName, int lineToRead)
{
	std::string line;
	std::ifstream myfile(fileToReadName);
	if (myfile.is_open())
	{
		for (int i = 0; i < lineToRead; i++)
		{
			std::getline(myfile, line);
		}
		myfile.close();
		return line;
	}
	else std::cout << "Unable to open file";

}
//creates vector of strings with comma seperated values.
std::vector<std::string> createObjectList(std::string fullobjectListLine, int numberOfVariables)
{
	std::string s = fullobjectListLine;
	std::string delimiter = ",";
	std::vector<std::string> fullList(numberOfVariables);

	size_t pos = 0;
	std::string token;
	int section = 0;
	while ((pos = s.find(delimiter)) != std::string::npos) 
	{
		token = s.substr(0, pos);
		fullList[section] = token;
		s.erase(0, pos + delimiter.length());
		section++;
	}
	//line below passes the remaining string after the last comma in.
	fullList[section] = s;
	return fullList;
}




//adds the values from a list of strings into the input playerStruct.
void DeserializeOppositionKnightValues(std::vector<std::string> oppositionKnightList, OppositionKnight &oppositionKnightToFill)
{
	oppositionKnightToFill.m_title = oppositionKnightList[0];
	oppositionKnightToFill.m_firstName = oppositionKnightList[1];
	oppositionKnightToFill.m_lastName = oppositionKnightList[2];
	oppositionKnightToFill.m_strength = std::stof(oppositionKnightList[3]);
	oppositionKnightToFill.m_stamina = std::stof(oppositionKnightList[4]);
	oppositionKnightToFill.m_agility = std::stof(oppositionKnightList[5]);

}
//this function will fill a vector of playerstructs with every entry in an entered csv file.
void FillOppositionKnightList(std::vector<OppositionKnight> & listOfOppositionKnights, std::string fileToFillFrom, int numberOfVariables)
{
	std::ifstream f(fileToFillFrom);
	std::string line;
	int lineNumberCount = 0;
	for (int i = 0; std::getline(f, line); ++i)
	{
		lineNumberCount++;
	}

	for (int i = 0; i < lineNumberCount; i++)
	{
		OppositionKnight newOpposingKnight;

		std::string newOpposingKnightLine = createStringWithSingleLine(fileToFillFrom, i+1);
		std::vector<std::string> oppositionKnightList = createObjectList(newOpposingKnightLine, numberOfVariables);

		DeserializeOppositionKnightValues(oppositionKnightList, newOpposingKnight);
		listOfOppositionKnights.push_back(newOpposingKnight);
	}




}





//adds the values from a list of strings into the input playerStruct.
void DeserializeOppositionKnightValues(std::vector<std::string> equipmentList, Equipment &EquipmentToFill)
{
	EquipmentToFill.m_name = equipmentList[0];
	EquipmentToFill.m_type = std::stoi(equipmentList[1]);
	EquipmentToFill.m_durability = std::stoi(equipmentList[2]);
	EquipmentToFill.m_effectiveness = std::stoi(equipmentList[3]);
	EquipmentToFill.m_cost = std::stoi(equipmentList[4]);
}
//this function will fill a vector of playerstructs with every entry in an entered csv file.
void FillEquipmentList(std::vector<Equipment> & listOfAllEquipment, std::string fileToFillFrom, int numberOfVariables)
{
	std::ifstream f(fileToFillFrom);
	std::string line;
	int lineNumberCount = 0;
	for (int i = 0; std::getline(f, line); ++i)
	{
		lineNumberCount++;
	}

	for (int i = 0; i < lineNumberCount; i++)
	{
		Equipment newEquipment;

		std::string newEquipmentLine = createStringWithSingleLine(fileToFillFrom, i + 1);
		std::vector<std::string> EquipmentList = createObjectList(newEquipmentLine, numberOfVariables);

		DeserializeOppositionKnightValues(EquipmentList, newEquipment);
		listOfAllEquipment.push_back(newEquipment);
	}




}
//This function will print to screen a list of equipment
void PrintEquipmentList(std::vector<Equipment> equipmentList)
{
	for (int i = 0; i < equipmentList.size(); i++)
	{
		if (equipmentList[i].m_type == 0)
		{
			std::cout << "item number " << i << std::endl;
			std::cout << equipmentList[i].m_name << ", class:" << equipmentList[i].m_effectiveness << ", cost: " << equipmentList[i].m_cost << std::endl;
			std::cout << std::endl;
		}
		else if (equipmentList[i].m_type == 1)
		{
			std::cout << "item number " << i << std::endl;
			std::cout << equipmentList[i].m_name << ", damage:" << equipmentList[i].m_effectiveness << ", cost: " << equipmentList[i].m_cost << std::endl;
			std::cout << std::endl;
		}
	}
}



void FirstScreen()
{
	std::cout << "Welcome, what is your Title knight?" << std::endl;
	std::cin >> playerCharacter.m_title;
	std::cout << std::endl;

	std::cout << "Welcome, what is your Name knight?" << std::endl;
	std::cin >> playerCharacter.m_name;

}

void EquipmentMenu()
{
	char MenuChoice = 0;
	int purchaseChoice = -1;
	int repairChoice = -1;
	std::string fileToRead;
	std::string fileToWrite;
	std::string messageToWrite;

	std::cout << "This is the equipment menu. \nPress B to go back. \nPress V to view your equipment.\nPress S to look at the store.\nPress R to repair your equipment." << std::endl;
	std::cin >> MenuChoice;

	if (MenuChoice == 'b' || MenuChoice == 'B')
	{

	}
	//code for viewing your equipment
	else if (MenuChoice == 'v' || MenuChoice == 'V')
	{
		playerCharacter.InvestigateEquipment();
	}
	//code for viewing the shop and buying new equipment
	else if (MenuChoice == 's' || MenuChoice == 'S')
	{
		PrintEquipmentList(allEquipment);
		std::cout << "You have a total curreny of " << playerCharacter.m_currency << std::endl;
		std::cout << "Enter the number of the item you want to buy" << std::endl;
		std::cin >> purchaseChoice;
		if (purchaseChoice > -1 && purchaseChoice < 16)
		{
			if (playerCharacter.m_currency > allEquipment[purchaseChoice].m_cost)
			{
				if (allEquipment[purchaseChoice].m_type == 0)
				{
					playerCharacter.m_armour = allEquipment[purchaseChoice];
					playerCharacter.m_currency -= allEquipment[purchaseChoice].m_cost;
				}
				else if (allEquipment[purchaseChoice].m_type == 1)
				{
					playerCharacter.m_weapon = allEquipment[purchaseChoice];
					playerCharacter.m_currency -= allEquipment[purchaseChoice].m_cost;
				}
				
			}
			else
			{
				std::cout << "You do not have enough money for this item" << std::endl;
			}

		}
		else
		{
			std::cout << "You made an invalid selection" << std::endl;
		}
	}
	//code for repairing your equipment.
	else if (MenuChoice == 'r' || MenuChoice == 'R')
	{
		std::cout << "Which piece of equipment would you like to repair?" << std::endl;
		std::cout << "Enter 1 for armour or 2 for weapon." << std::endl;
		std::cin >> repairChoice;
		if (repairChoice == 1)
		{
			if ((playerCharacter.m_currency -= (99 - playerCharacter.m_armour.m_durability)) > 0)
			{
				if (playerCharacter.m_armour.m_type == -1)
				{
					std::cout << "You have no armour" << std::endl;
				}
				else
				{
					playerCharacter.m_currency -= (99 - playerCharacter.m_armour.m_durability);
					playerCharacter.m_armour.m_durability = 99;
					std::cout << "You have repaird your armour, your money is now " << playerCharacter.m_currency << std::endl;
				}
			}
			else
			{
				std::cout << "You do not have enough money" << std::endl;
			}
		}
		else if (repairChoice == 2)
		{
			if ((playerCharacter.m_currency -= (99 - playerCharacter.m_armour.m_durability)) > 0)
			{
				if (playerCharacter.m_weapon.m_type == -1)
				{
					std::cout << "You have no weapon" << std::endl;
				}
				else
				{
					playerCharacter.m_currency -= (99 - playerCharacter.m_weapon.m_durability);
					playerCharacter.m_weapon.m_durability = 99;
					std::cout << "You have repaird your weapon, your money is now " << playerCharacter.m_currency << std::endl;
				}
			}
			else
			{
				std::cout << "You do not have enough money" << std::endl;
			}
		}
		else
		{
			std::cout << "You have made an invalid selection" << std::endl;
		}
	}
	else
	{
		std::cout << "You have made an invalid selection" << std::endl;
	}

}


//this function will control the main menu with multiple choices to go further
void MainMenu()
{
	char MenuChoice = 0;
	std::string fileToRead;
	std::string fileToWrite;
	std::string messageToWrite;

	int lineNumber = 0;
	int variableNumber = 0;


	std::cin.clear();
	std::cout << "This is the main menu. \nPress Q to quit. \nPress E to deal with your equipment.\nPress L to Level up.\nPress F to fight your enemy." << std::endl;
	std::cin >> MenuChoice;

	if (MenuChoice == 'q' || MenuChoice == 'Q')
	{
 		runProgram = false;
	}
	
	else if (MenuChoice == 'e' || MenuChoice == 'E')
	{
		EquipmentMenu();
	}

	else if (MenuChoice == 'l' || MenuChoice == 'L')
	{
		if (playerCharacter.m_skillPoints > 0)
		{
			int levelUpChoice = -1;

			std::cout << "Enter a key to level up one of your attributes.\n1 = strength, \n2 = stamina, \n3 = agility" << std::endl;
			playerCharacter.PrintAttributes();
			playerCharacter.PrintSkillPoints();
			std::cin >> levelUpChoice;


			if (levelUpChoice == 1)
			{
				playerCharacter.m_strength += 1;
				playerCharacter.m_skillPoints -= 1;
			}
			else if (levelUpChoice == 2)
			{
				playerCharacter.m_stamina += 1;
				playerCharacter.m_skillPoints -= 1;
			}
			else if (levelUpChoice == 3)
			{
				playerCharacter.m_agility += 1;
				playerCharacter.m_skillPoints -= 1;
			}
			else
			{
				std::cout << "You have entered an invalid command, please try again." << std::endl;
			}
		}
		else
		{
			std::cout << "You do not have enough skill points" << std::endl;
		}
	}

	else if (MenuChoice == 'f' || MenuChoice == 'F')
	{
		int fightChoice;
		std::vector<OppositionKnight> currentOptionsToFight = combatManager.DisplayListOfFive(allOppositionKnights);
		std::cout << "Enter your choice" << std::endl;
		std::cin >> fightChoice;
		if (fightChoice < 0 || fightChoice > 4)
		{
			std::cout << "You have entered an invalid command, please try again." << std::endl;
		}
		else
		{
			runProgram = combatManager.SimulateFight(&playerCharacter, currentOptionsToFight[fightChoice]);
		}


	}

	else
	{
		std::cout << "You have entered an invalid command, please try again." << std::endl;
	}

}









int main() 
{


	FillOppositionKnightList(allOppositionKnights, "contenders.csv", 6);
	FillEquipmentList(allEquipment, "loadout.csv", 5);
 	runProgram = true;
	FirstScreen();
	while (runProgram)
	{
	std::srand((unsigned int)std::time(0));
	std::rand();
	MainMenu();
	}

	return 0;
	
	
	


}


