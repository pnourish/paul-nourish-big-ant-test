#include "Equipment.h"



Equipment::Equipment()
{
	m_type = -1;
	m_durability = 99;
}




Equipment::~Equipment()
{
}

void Equipment::ShowStats()
{
	if (m_type == 0) 
	{
		std::cout << "Your armour's name is " << m_name.c_str() << std::endl;
		std::cout << "Your armour's durability is " << m_durability << std::endl;
		std::cout << "Your armour's class is " << m_effectiveness << std::endl;
		std::cout << "Your armour's cost was " << m_cost << std::endl;
	}
	else if(m_type == 1)
	{
		std::cout << "Your weapon's name is " << m_name.c_str() << std::endl;
		std::cout << "Your weapon's durability is " << m_durability << std::endl;
		std::cout << "Your weapon's damage is " << m_effectiveness << std::endl;
		std::cout << "Your weapon's cost was " << m_cost << std::endl;
	}
	else
	{
		std::cout << "You have no equipment " << std::endl;
	}
}
