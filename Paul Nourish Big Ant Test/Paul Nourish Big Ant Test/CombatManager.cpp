#include "CombatManager.h"



CombatManager::CombatManager()
{
}


CombatManager::~CombatManager()
{
}

std::vector<OppositionKnight> CombatManager::DisplayListOfFive(std::vector<OppositionKnight> fullOppositionKnightList)
{
	std::vector<OppositionKnight> tempKnightOppositionList = fullOppositionKnightList;
	int numberOfKnightsSelected = 0;
	std::vector<OppositionKnight> knightsSelected;
	while (knightsSelected.size() < 5)
	{
		int nextPick = std::rand() % tempKnightOppositionList.size();
		knightsSelected.push_back(tempKnightOppositionList[nextPick]);
		tempKnightOppositionList.erase(tempKnightOppositionList.begin() + nextPick);
	}

	for (int i = 0; i < knightsSelected.size(); i++)
	{
		std::cout << "Option number " << i << " for you to fight is" << std::endl;
		knightsSelected[i].PrintDetails();
		std::cout << std::endl;
	}
	return knightsSelected;
}

bool CombatManager::SimulateFight(Knight* playerCharacter, OppositionKnight EnemyToFight)
{
	fightHappening = true;
	while (fightHappening)
	{
		SimulateRound(playerCharacter, &EnemyToFight);

		if (playerCharacter->m_stamina <= 0)
		{
			std::cout << "You have been defeated, the peasents shall clean up your remains" << std::endl;
			system("pause");
			fightHappening = false;
			return false;
		}

		if (playerCharacter->m_stamina <= 0 && EnemyToFight.m_stamina <= 0)
		{
			std::cout << "You have been defeated, but atleast you took your enemy with you" << std::endl;
			system("pause");
			fightHappening = false;
			return false;
		}

		if (EnemyToFight.m_stamina <= 0)
		{
			std::cout << "Your enemy lays dead before you, enjoy your rewards." << std::endl;
			int moneyWon = (EnemyToFight.m_strength + EnemyToFight.m_agility) * 10;
			system("pause");
			playerCharacter->m_currency += moneyWon;
			playerCharacter->m_skillPoints += 5;
			fightHappening = false;
			return true;
		}
	}

}

void CombatManager::SimulateRound(Knight * playerCharacter, OppositionKnight* EnemyToFight)
{
	float playerAttackValue;
	float playerDefenceValue;

	float oppositionAttackValue;
	int playerDamageModifier = (rand() % 20) - 10;

	playerAttackValue = (playerCharacter->m_weapon.m_effectiveness + playerDamageModifier) + (playerCharacter->m_agility) + (playerCharacter->m_strength / 10);
	playerAttackValue = playerAttackValue + (playerCharacter->m_weapon.m_durability / 100);

	playerDefenceValue = playerCharacter->m_armour.m_effectiveness /10;

	oppositionAttackValue = (EnemyToFight->m_strength + EnemyToFight->m_agility) / 10;


	EnemyToFight->m_stamina -= playerAttackValue;
	playerCharacter->m_stamina -= oppositionAttackValue - playerDefenceValue;

	std::cout << playerCharacter->m_title.c_str() << " " << playerCharacter->m_name.c_str() << " has " << playerCharacter->m_stamina << " stamina left" << std::endl;
	std::cout << EnemyToFight->m_title.c_str() << " " << EnemyToFight->m_firstName.c_str() << " " << EnemyToFight->m_lastName.c_str() <<" has " << EnemyToFight->m_stamina << " stamina left" << std::endl;

	playerCharacter->m_weapon.m_durability -= 1;
	playerCharacter->m_armour.m_durability -= 1;
}
