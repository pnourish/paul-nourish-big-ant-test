#pragma once
#include <iostream>
#include <vector>

#include "OppositionKnight.h"
#include "Knight.h"
class CombatManager
{
public:
	CombatManager();
	~CombatManager();


	std::vector<OppositionKnight> DisplayListOfFive(std::vector<OppositionKnight> fullOppositionKnightList);

	bool SimulateFight(Knight* playerCharacter, OppositionKnight EnemyToFight);
	void SimulateRound(Knight* playerCharacter, OppositionKnight* EnemyToFight);
	bool fightHappening = false;
};

