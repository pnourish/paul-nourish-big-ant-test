#include "Knight.h"



Knight::Knight()
{

	m_strength = 10;
	m_stamina = 10;
	m_agility = 10;

	m_currency = 3000;
	m_skillPoints = 5;
}


Knight::~Knight()
{
}

void Knight::InvestigateEquipment()
{
	
	m_weapon.ShowStats();
	m_armour.ShowStats();

}

void Knight::PrintAttributes()
{
	std::cout << "Your current strength is " << m_strength << std::endl;
	std::cout << "Your current stamina is " << m_stamina<< std::endl;
	std::cout << "Your current agility is " << m_agility << std::endl;
}

void Knight::PrintSkillPoints()
{
	std::cout << "You have " << m_skillPoints << " skill points "<< std::endl;

}
