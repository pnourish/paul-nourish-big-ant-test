#include "OppositionKnight.h"



OppositionKnight::OppositionKnight()
{
	m_title = "/0";
	m_firstName = "/0";
	m_lastName = "/0";
	m_strength= 0;
	m_stamina = 0;
	m_agility = 0;
}


OppositionKnight::~OppositionKnight()
{
}

void OppositionKnight::PrintDetails()
{
	std::cout << m_title.c_str() << " " << m_firstName.c_str() << " " << m_lastName.c_str() << " " << "\nStrength: " << m_strength << "\nStamina: " << m_stamina << "\nAgility: " << m_agility << std::endl;
}
