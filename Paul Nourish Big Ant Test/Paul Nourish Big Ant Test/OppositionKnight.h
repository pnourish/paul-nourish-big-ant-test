#pragma once
#include <iostream>

class OppositionKnight
{
public:
	OppositionKnight();
	~OppositionKnight();

	void PrintDetails();


	std::string m_title;
	std::string m_firstName;
	std::string m_lastName;
	
	
	float m_strength;
	float m_stamina; 
	float m_agility;
};

