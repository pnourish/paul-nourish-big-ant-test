#pragma once
#include <iostream>
#include "Equipment.h"



class Knight
{
public:
	Knight();
	~Knight();


	void InvestigateEquipment();
	void PrintAttributes();
	void PrintSkillPoints();


	std::string m_title;
	std::string m_name;

	Equipment m_weapon;
	Equipment m_armour;

	float m_strength;
	float m_stamina;
	float m_agility;

	int m_currency;

	int m_skillPoints;

};

